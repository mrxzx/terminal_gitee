import sys
import re
import urllib.request
from bs4 import BeautifulSoup

def main():
    #先访问码云的热门项目页面
    html = urllib.request.urlopen("https://gitee.com/explore/starred").read().decode("utf8")
    #开始解析
    soup_html = BeautifulSoup(
        html,
        "html.parser"
    )
    #找到分类的div
    label_list = str(soup_html.find("div",{"class":"code-tab border-dashed","id":"languages-tab"}))
    soup_label = BeautifulSoup(
        label_list,
        "html.parser"
    )
    #拿到所有分类
    labels = soup_label.findAll("a")
    i = 1
    #让用户选择一个查看
    for label in labels:
        label_name = label.text.replace('\n','')
        print(str(i) + ":" + label_name)
        i = i + 1
    u_num = input("请选择查看分类(1,31暂不可用):")
    try:
        #判断用户输入的选项
        if int(u_num) > len(labels) or int(u_num) < 1:
            print("对不起!您的选择不存在!")
        else:
            u_label = labels[int(u_num) - 1].text.replace('\n','')
            get_project_list(1,u_label)

    except ValueError:
        print("对不起!您需要输入一个数字!")


def get_project_list(page, u_label):
    # 开始爬取具体内容
    label_html = urllib.request.urlopen("https://gitee.com/explore/starred?lang=" + u_label + "&page=" + str(page)).read().decode("utf8")
    soup_lprj = BeautifulSoup(
        label_html,
        "html.parser"
    )
    label_prj = str(soup_lprj.find("div", {"class": "ui relaxed divided items"}))
    soup_prjl = BeautifulSoup(
        label_prj,
        "html.parser"
    )
    where = 1;
    prj_list = soup_prjl.findAll("div", {"class": "item"})
    for list in prj_list:
        soup_prin = BeautifulSoup(
            str(list),
            "html.parser"
        )
        prj_info = soup_prin.find("div", {"class": "project-desc"}).text.replace("\n", ' ')
        if len(prj_info) > 50:
            prj_info = prj_info[:50] + "..."
        print(str(where) + ":" + prj_info)
        where = where + 1
    while True:
        c_prj = input("请选择需要的项目详细信息(下一页:n)")
        try:
            if c_prj == "n" or c_prj == "N":
                print("\n-----第" + str(page + 1) + "页-----\n")
                get_project_list(page + 1, u_label)
            elif int(c_prj) > len(prj_list) or int(c_prj) < 1:
                print("对不起！您的选择不存在！")
            else:
                file_name = input("请输入存放具体信息的文件名:")
                prj_info = prj_list[int(c_prj) - 1]
                soup_data = BeautifulSoup(
                    str(prj_info),
                    "html.parser"
                )
                prj_url = soup_data.find("a",{"class":"title project-namespace-path"})["href"]
                prj_url = "https://gitee.com/" + prj_url
                prj_info_l = urllib.request.urlopen(prj_url).read().decode("utf8")
                soup_info = BeautifulSoup(
                    prj_info_l,
                    "html.parser"
                )
                prj_about = soup_info.find("div",{"class":"file_content markdown-body"})
                prj_urls = soup_info.find("input",{"id":"project_clone_url"})["value"]
                dr = re.compile(r'<[^>]+>', re.S)
                prj_about = dr.sub('',str(prj_about))
                with open(file_name,"w") as f:
                    f.write("git地址:" + str(prj_url) + "\n\n")
                    f.write(str(prj_about))
                print("写入成功！程序自动退出")
                sys.exit(0)

        except ValueError:
            print("对不起!您需要输入一个数字!")


if __name__ == "__main__":
    main()